set nocompatible
set expandtab

set tabstop=4 " A tab is 4 spaces
set softtabstop=4
set shiftwidth=4 " Number of spaces to use for autoindenting
set autoindent
set textwidth=140
set hidden
set backspace=indent,eol,start " Allow backspacig over anythig in insert mode
set showmatch " Set show matchig parenthesis
set ignorecase " Ignore case when searching
set smartcase " Ignore case if search pattern is all lowercase, case-sensitive otherwise
set smarttab " Insert tabs o nthe start of a line according to shiftwidth, not tabstop
set hlsearch " Highlight search terms
set incsearch " Show search matches as you type

set foldenable " Enable folding
set foldlevelstart=10 " Open most folds by default
set foldnestmax=10 " 10 nested fold maximum
set foldmethod=indent " Fold based on indent level

set mouse=a "adds mouse control like windows

set backup
set backupdir =~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup


set relativenumber " Always show line number in relative order

set wildmenu " Visual autocomplete for command menu
set lazyredraw " Redraw only when we need to

set history=1000 " Remember more commands nd search history
set undolevels=1000 " Undo upto 1000 things
set title " Change the terminal's title
set novisualbell " don't beep
set noerrorbells "don't beep


set pastetoggle=<F2>

filetype indent on " Load filetype-specific indent files


func! WordProcessorMode()
    setlocal textwidth=80
    setlocal smartindent
    setlocal spell spelllang=en_us
    setlocal noexpandtab
endfu

com! WP call WordProcessorMode()

filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'L9'

call vundle#end()
filetype plugin indent on

" ----------------------------------------
" Syntax: highlighting
" ---------------------------------------
if has('syntax')
    syntax enable               " Turn on syntax highlighting
    silent! colorscheme eldar   " Custom color scheme
endif
